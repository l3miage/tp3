package exer2;

import java.util.Scanner;

public class InsertionInteger {

    private static final int MAX_SIZE = 10;
    private int size = 0;
    private int[] array = new int[MAX_SIZE];

    public InsertionInteger() {
        this.size = 0;
        /*try (Scanner scanner = new Scanner(System.in)){
            createArray(scanner);
        }*/
    }

    public int[] toArray() {
        int[] copy = new int[size];
        if (size > 0) {
            for (int i = 0; i < this.size; i++) {
                copy[i] = this.array[i];
            }
        }
        return copy;
    }

    public boolean insert(int value) {
        boolean result = false;
        if (this.size != MAX_SIZE) {
            int i = 0;
            boolean fini = false;
            while (i < this.size && !fini) {
                if (this.array[i] < value) {
                    i++;
                } else if (this.array[i] == value) {
                    fini = true;
                } else {
                    this.size ++;
                    if (this.size - 1 - i >= 0) System.arraycopy(this.array, i, this.array, i + 1, this.size - 1 - i);
                    this.array[i] = value;
                    result = true;
                    fini = true;
                }
            }
            if (i == this.size) {
                this.array[this.size] = value;
                this.size ++;
                result = true;
            }

        }

        return result;
    }

    public String toString() {
        String ret = "Voici la liste triée : [";
        for (int i = 0; i < this.size-1; i++) {
            ret = ret + this.array[i] + ", ";
        }
        ret = ret + this.array[this.size-1] + "]";
        return ret;
    }

    public void createArray(Scanner scanner) {
        int entry = 0;
        while(entry != -1 && this.size < MAX_SIZE) {
            //System.out.print("Veuillez saisir un entier : ");
            entry = scanner.nextInt();
            if (entry != -1) this.insert(entry);
        }
        System.out.println(this.toString());
    }

}
