package exer3;

public class Pair{

    protected int value1, value2;

    public Pair(int value1, int value2){
        this.value1 = value1;
        this.value2 = value2;
    }

    public Pair(Pair p){
        this.value1 = p.value1;
        this.value2 = p.value2;
    }

    public boolean equals(Pair p) {
        if (p == null) {
            return false;
        }
        else {
            if (this.value1 == p.value1 && this.value2 == p.value2) {
                return true;
            } else {
                return false;
            }
        }
    }

    public String toString() {
        return "[" + this.value1 + ", " + this.value2 + "]";
    }

    public boolean less(Pair p) {
        if (p == null) {
            return false;
        } else {
            if (this.value1 < p.value1 || this.value1 == p.value1 && this.value2 < p.value2){
                return true;
            } else {
                return false;
            }
        }
    }
}
