package exer3;

import java.util.Scanner;

public class InsertionPair {

    private static final int SIZE_MAX = 10;
    private int size = 0;
    private Pair[] array = new Pair[SIZE_MAX];

    public InsertionPair(){
        this.size = 0;
        /*try (Scanner scanner = new Scanner(System.in)){
            createArray(scanner);
        }*/
    }

    public Pair[] toArray(){
        Pair[] copy = new Pair[size];
        if (size > 0) {
            for (int i = 0; i < this.size; i++) {
                Pair p = new Pair(this.array[i]);
                copy[i] = p;
            }
        }
        return copy;
    }

    public boolean insert(Pair toInsert) {
        boolean result = false;
        if (this.size != SIZE_MAX && toInsert.value1 != -1 && toInsert.value2 != -1) {
            int i = 0;
            boolean fini = false;
            while (i < this.size && !fini) {
                if (this.array[i].less(toInsert)) {
                    i++;
                } else if (this.array[i].equals(toInsert)) {
                    fini = true;
                } else {
                    this.size ++;
                    if (this.size - 1 - i >= 0) System.arraycopy(this.array, i, this.array, i + 1, this.size - 1 - i);
                    this.array[i] = toInsert;
                    result = true;
                    fini = true;
                }
            }
            if (i == this.size) {
                this.array[this.size] = toInsert;
                this.size ++;
                result = true;
            }

        }

        return result;
    }

    public String toString() {
        String ret = "Voici la liste triée : ";
        for (int i = 0; i < this.size-1; i++) {
            ret = ret + this.array[i].toString() + ", ";
        }
        if (this.size > 0) ret = ret + this.array[this.size-1].toString() + ".";
        return ret;
    }

    public void createArray(Scanner scanner) {
        int second = 0;
        int first;
        boolean fini = false;
        while ((second != -1) && !fini) {
            //System.out.print("Veuillez saisir un entier : ");
            first = scanner.nextInt();
            if (first != -1) {
                //System.out.print("Veuillez saisir un second entier : ");
                second = scanner.nextInt();
                Pair p = new Pair(first, second);
                this.insert(p);
            } else {
                fini = true;
            }
        }
        System.out.println(this.toString());
    }

}
