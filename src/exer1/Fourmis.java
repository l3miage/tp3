package exer1;

public class Fourmis {

    public Fourmis() {}

    public static void main(String[] args) {
        String suite = "1";
        System.out.println(suite);
        while (suite.length() <= 10) {
            suite = next(suite);
            System.out.println(suite);
        }
    }

    public static String next(String ui) {
        int cpt = 1;
        StringBuilder temp = new StringBuilder();
        for (int i = 1; i < ui.length(); i++) {
            if (ui.charAt(i) == ui.charAt(i - 1)) {
                cpt++;
            } else {
                temp.append(cpt).append(ui.charAt(i - 1));
                cpt = 1;
            }
        }
        temp.append(cpt).append(ui.charAt(ui.length() - 1));
        return temp.toString();
    }

}
